<?php
namespace Drupal\user_signup\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\jwt\Transcoder\JwtTranscoderInterface;
use Drupal\jwt\Transcoder\JwtTranscoder;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to register a new user in the system.
 *
 * @RestResource(
 *   id = "user_register_rest_resource",
 *   label = @Translation("Register User rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/signup",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/signup"
 *   }
 * )
 */

class UserSignUpResource extends ResourceBase {
    /**
     * The JWT Transcoder service.
     *
     * @var \Drupal\jwt\Transcoder\JwtTranscoderInterface
     */
    protected $transcoder;

    /**
     * The event dispatcher.
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $eventDispatcher;


    /**
     * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The config factory.
     * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
     *   The link relation type manager.
     */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      LoggerInterface $logger) {
      parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
      $this->eventDispatcher = \Drupal::service('event_dispatcher');
      $this->transcoder = new JwtTranscoder(new \Firebase\JWT\JWT(), \Drupal::configFactory(), \Drupal::service('key.repository'));
    }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      if(!$this->emailExists($content->email)) {
        $data = array();
        $data['user'] = $this->createUser($content);
        $data['token'] = $this->generateToken();
        return new ResourceResponse($data, 201);
      }
      else {
        return new ResourceResponse(['error' => 'This email already exists. Try login in']);
      }
    }
    return new ResourceResponse(['error'=>'Bad Request'], 400);
  }

  /**
   * Generates a new JWT.
   */
  public function generateToken() {
    $event = new JwtAuthGenerateEvent(new JsonWebToken());
    $this->eventDispatcher->dispatch(JwtAuthEvents::GENERATE, $event);
    $jwt = $event->getToken();
    return $this->transcoder->encode($jwt);
  }

  public function validate($content)
  {
    return isset($content->first_name, $content->last_name, $content->email, $content->password,$content->mobile_number) && !empty($content->first_name) && !empty($content->last_name) && !empty($content->mobile_number) && !empty($content->email) && !empty($content->password)
    && filter_var($content->email, FILTER_VALIDATE_EMAIL);
  }
  public function createUser($content)
  {
    $user = \Drupal\user\Entity\User::create();
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $user->setPassword($content->password);
    $user->enforceIsNew();
    $user->setEmail($content->email);
    $user->setUsername($content->email);
    $user->set('init', 'email');
    $user->set('langcode', $language);
    $user->set('preferred_langcode', $language);
    $user->set('preferred_admin_langcode', $language);
    $user->activate();
    $user->set('field_first_name',$content->first_name);
    $user->set('field_last_name',$content->last_name);
    $user->set('field_mobile_number',$content->mobile_number);
    // $result = $user->save();
    $user->save();
    $temp_user = array();
    $temp_user['id'] = $user->Id();
    $temp_user['username'] = $user->getUsername();
    $temp_user['email'] = $user->getEmail();
    $temp_user['first_name'] = $user->get('field_first_name')->value;
    $temp_user['last_name'] = $user->get('field_last_name')->value;
    $temp_user['mobile_number'] = $user->get('field_mobile_number')->value;
    $accountSwitcher = \Drupal::service('account_switcher');
    $accountSwitcher->switchTo($user);
    $result = $this->sendMail($user);
    return $temp_user;
  }

  public function emailExists($email)
  {
    $users = \Drupal::entityManager()->getStorage('user')->loadByProperties(['mail' => $email]);
    $user = reset($users);
    if ($user) {
      return true;
    }
    else {
      return false;
    }
  }

  public function sendMail($user)
  {
    $from = Null;
    $to = $user->getEmail();
    $random = rand(10000,99999);
    $subject = "Congrats! you have successfully registered yourself on UshaCook";
    $body = "Congrats! you have successfully registered yourself on UshaCook.Here is the OTP for email verfication ".$random;
    $user->set('field_email_verification_code',$random);
    $user->save();
    user_signup_send($from, $to, $subject, $body);
    return true;
  }
}
