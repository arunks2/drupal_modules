<?php
namespace Drupal\poll_post_choice\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;



/**
 * Provides a resource to get a poll question.
 *
 * @RestResource(
 *   id = "poll_choice_rest_resource",
 *   label = @Translation("Poll Choice rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/poll/choice",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/poll/choice"
 *   }
 * )
 */

class ChoiceResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    $choice_id = $content->choice_id;
    $poll_id = $content->poll_id;
    $ip = \Drupal::request()->getClientIp();
    $connection = \Drupal::database();
    $query = $connection->query("SELECT COUNT(*) FROM {poll_vote} WHERE hostname = :hostname and pid = :pid limit 1", array(':hostname' => $ip, ':pid' => $poll_id));
    $results = $query->fetch();
    $ifEntryExists = false;
    foreach($results as $c){
        $ifEntryExists = intval($c); // check if this hostname has already made his/her choices
    }
    if(!$ifEntryExists) { // host has not made any choice on this poll, lets create a resource for his/her choice
      $query = $connection->query("select COUNT(*) from {poll__choice} where entity_id = :eid AND choice_target_id = :chid",
        array(':eid' => $poll_id,':chid' => $choice_id));
      $pollHasThisChoice =  false;
      foreach($query->fetch() as $r) {
        $pollHasThisChoice = intval($r); // check if the current choice exists on this poll
      }
      if($pollHasThisChoice) { // yes, current choice exists on current poll now insert this into {poll_vote} table
        $result = $connection->insert('poll_vote')
                                                ->fields([
                                                  'pid' => $poll_id,
                                                  'uid' => 0,
                                                  'chid' =>   $choice_id,
                                                  'hostname' => $ip, 
                                                  'timestamp' => REQUEST_TIME
                                                ])
                                                  ->execute();
        $data['result'] = $result;
        return new ResourceResponse($data, 201); // return with the newly created resource
      }
      else { // this poll has no such choice, may be log this mallicious attempt in future
        $data['error'] = "No such choice for the current poll";
        return new ResourceResponse($data, 404);
      }
    }
    else { // The current host has already made his choice on current poll
      $data['error'] = "Choice already Made";
      return new ResourceResponse($data, 400);
    }
  }
}
