<?php
namespace Drupal\update_store\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\node\Entity\Node;

/**
 * Provides a resource to save stores in the system.
 *
 * @RestResource(
 *   id = "update_stores_rest_resource",
 *   label = @Translation("Update Stores rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/stores/update",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/stores/update"
 *   }
 * )
 */

class AddCategoryToStoreResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      if($content->type == 'update') {
        $result = $this->updateStores($content);
        return new ResourceResponse(['success' => $result],200);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->store_name, $content->type,$content->mobile_number) && !empty($content->store_name) && !empty($content->mobile_number);
  }

  public function updateStores($content)
  {
    $category  =  \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $content->store_name]);
    $stores = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['field_store_mobile_number' => $content->mobile_number]);
    if($store = reset($stores)) {
      $store->field_store_categories = $category;
      $store->save();
      return true;
    }
  }
}
