<?php
namespace Drupal\user_recipecollections\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to get the current user`s saved Recipe Collections in the system.
 *
 * @RestResource(
 *   id = "get_user_recipecollections_rest_resource",
 *   label = @Translation("Get User Recipe Collection rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/recipecollections",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/recipecollections"
 *   }
 * )
 */

class UserRecipeCollectionResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity get requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    $currentUser = \Drupal::currentUser();
    $user = \Drupal\user\Entity\User::load($currentUser->Id());
    $recipecollections = $user->get('field_user_recipe_collections')->entity;
    $data['recipecollections'] = $recipecollections;
    return new ResourceResponse($data);
  }
}
