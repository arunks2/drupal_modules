<?php
namespace Drupal\user_recipes\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to get the current user`s Recipes in the system.
 *
 * @RestResource(
 *   id = "get_user_recipes_rest_resource",
 *   label = @Translation("Get User Recipes rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/recipes",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/recipes"
 *   }
 * )
 */

class UserRecipesResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity get requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    $currentUser = \Drupal::currentUser();
    $user = \Drupal\user\Entity\User::load($currentUser->Id());
    $recipe_book = $user->get('field_user_recipe_books')->entity;
    // var_dump($recipes->field_recipe_bok_recipes[0]->target_id);
    // die();
    $recipesDetails = array();
    $recipesDetails['name'] = array();
    $recipesDetails['images'] = array();
    $recipesDetails['details'] = array();
    $recipesDetails['nid'] = array();
    foreach($recipe_book->field_recipe_bok_recipes as $r) {
      $recipes = \Drupal\node\Entity\Node::load($r->target_id)->toArray();
      $recipesDetails['name'] = $recipes->nid;
      $recipesDetails['images'] = $recipes->title;
      $recipesDetails['nid'] = $recipes->nid;
      // $recipesDetails[]
    }
    // $recipe_book['recipes'] = $recipesDetails;
    $data['recipe_books'] = $recipe_book;
    return new ResourceResponse($data);
  }
}
