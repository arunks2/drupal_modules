<?php
namespace Drupal\user_article_remove\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\user\Entity\User;
use \Drupal\node\Entity\Node;

/**
 * Provides a resource to remove an article for the current user in the system.
 *
 * @RestResource(
 *   id = "remove_user_article_rest_resource",
 *   label = @Translation("Remove User Article rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/article/delete",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/article/delete"
 *   }
 * )
 */

class UserArticleRemoveResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $user = User::load($currentUser->Id());
      $article = Node::load($content->article_id);
      if($article->getType() != 'article') {
        return new ResourceResponse(['error' => 'Unprocessable Entity'],422);
      }
      foreach($user->field_user_articles as $delta => $item)  {
        if ($item->target_id == $content->article_id) {
          unset($user->field_user_articles[$delta]);
          $user->save();
          break;
        }
      }
      return new ResourceResponse(['success' =>'deleted'], 201);
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->article_id) && !empty($content->article_id);
  }
}
