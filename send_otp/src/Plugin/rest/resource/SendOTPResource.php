<?php
namespace Drupal\send_otp\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to send otp for forget password the current user in the system.
 *
 * @RestResource(
 *   id = "send_otp_code_password_user_rest_resource",
 *   label = @Translation("Send the User code for forget password rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/forgetpassword",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/forgetpassword"
 *   }
 * )
 */

class SendOTPResource extends ResourceBase {
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['mail' => $content->email]);
      $user = reset($users);
      if ($user) {
        $uid = $user->id();
        $user =  \Drupal::entityTypeManager()->getStorage('user')->load($uid);
        $result = $this->sendMail($user);
        return new ResourceResponse($result,201);
      }
      else {
        return new ResourceResponse(['error' => 'NO_SUCH_USER_EXISTS_IN_SYSTEM'], 404);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->email) && !empty($content->email) && filter_var($content->email, FILTER_VALIDATE_EMAIL);
  }
  public function sendMail($user)
  {
    $from = Null;
    $to = $user->getEmail();
    $random = rand(10000,99999);
    $subject = "OTP for forget password request";
    $body = "The OTP for password change is ".$random;
    $user->set('field_forget_password_code',$random);
    $user->save();
    user_signup_send($from, $to, $subject, $body);
    return true;
  }
}
