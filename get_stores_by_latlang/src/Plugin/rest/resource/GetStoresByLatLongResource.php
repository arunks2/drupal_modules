<?php
namespace Drupal\get_stores_by_latlang\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\node\Entity\Node;


/**
 * Provides a resource to get the stores nearest to a given distance using Lat and Lang.
 *
 * @RestResource(
 *   id = "get_stores_by_latlang_rest_resource",
 *   label = @Translation("Get Stores by Lat and Lang rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/stores/latlang",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/stores/latlang"
 *   }
 * )
 */

class GetStoresByLatLongResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $store = \Drupal::entityTypeManager()
                                    ->getStorage('node')
                                    ->loadByProperties(['field_store_postal_code' => $content->postal_code]);
      $store = reset($store);
      $s = array();
      if($store) {
          $s['id']   = $store->id();
          $s['title']  = $store->get('title')->value;
          $s['lat']   = $store->get('field_store_lat')->value;
          $s['lang']  = $store->get('field_store_lang')->value;
          $s['long'] = $store->get('field_store_lang')->value;
          $s['mobile_number'] = $store->get('field_store_mobile_number')->value;
          $s['state'] = $store->get('field_store_states')->entity->name;
          $s['city'] = $store->get('field_store_cities')->entity->name;
          $s['postal_code'] = $store->get('field_store_postal_code')->value;
          $s['timings'] = $store->get('field_store_timings')->value;
          $s['closed_on'] = $store->get('field_closed_on')->entity->name;
          $s['nearest_stores'] = $this->getNearestStores($s, $content);
          return new ResourceResponse($s, 200);
      }
      else {
        $s['lang']  = $content->lang;
        $s['lat'] = $content->lat;
        $s['nearest_stores'] = $this->getNearestStores($s, $content);
        return new ResourceResponse($s, 200);
      }
    }
    return new ResourceResponse(['error' => 'Bad Request'], 400);
  }

  public function validate($content)
  {
    return isset($content->postal_code) && !empty($content->postal_code);
  }

  public function getNearestStores($s, $content)
  {
    $nids = \Drupal::entityQuery('node')->condition('type','stores')->execute();
    $latitude1 = $s['lat'];
    $longitude1 = $s['lang'];
    $nearest_stores = array();
    $nearest_stores['title'] = array();
    $nearest_stores['id'] = array();
    $nearest_stores['lat'] = array();
    $nearest_stores['long'] = array();
    $nearest_stores['timings'] = array();
    $nearest_stores['mobile_number'] = array();
    $nearest_stores['state'] = array();
    $nearest_stores['city'] = array();
    $nearest_stores['closed_on'] = array();
    $nearest_stores['postal_code'] = array();
    $nearest_stores['address'] = array();
    $nearest_stores['distance'] = array();
     foreach($nids as $nid) {
      $store = Node::load($nid);
      $latitude2 = $store->get('field_store_lat')->value;
      $longitude2 = $store->get('field_store_lang')->value;
      $result = $this->ifInDistance($latitude1, $longitude1, $latitude2,$longitude2,$content->distance);
      if($result) {
        $nearest_stores['title'][] = $store->get('title')->value;
        $nearest_stores['id'][] = $store->id();
        $nearest_stores['lat'][] = $store->get('field_store_lat')->value;
        $nearest_stores['long'][] = $store->get('field_store_lang')->value;
        $nearest_stores['mobile_number'][] = $store->get('field_store_mobile_number')->value;
        $nearest_stores['state'][] = $store->get('field_store_states')->entity->name;
        $nearest_stores['city'][] = $store->get('field_store_cities')->entity->name;
        $nearest_stores['postal_code'][] = $store->get('field_store_postal_code')->value;
        $nearest_stores['timings'][] = $store->get('field_store_timings')->value;
        $nearest_stores['closed_on'][] = $store->get('field_closed_on')->entity->name;
        $nearest_stores['address'][] = $store->get('field_store_full_address')->value;
        $nearest_stores['distance'][] = $result;
       }
    }
    return $nearest_stores;
  }
  public function ifInDistance($latitude1, $longitude1, $latitude2, $longitude2,$distance)
  {
    $earth_radius = 6371; 
    $dLat = deg2rad($latitude2 - $latitude1);
    $dLon = deg2rad($longitude2 - $longitude1);
    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
    $c = 2 * asin(sqrt($a));
    $d = $earth_radius * $c;
    if($d <= $distance) {
      return $d;
    }
    return false;
  }
}
