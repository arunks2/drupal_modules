<?php
namespace Drupal\subscriber_entity\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to save the subscriber`s email in the system.
 *
 * @RestResource(
 *   id = "subscriber_rest_resource",
 *   label = @Translation("Subscriber rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/subscriber",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/subscriber"
 *   }
 * )
 */

class SubscriberResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $connection = \Drupal::database();
      $query = $connection->query("SELECT COUNT(*) FROM {contact} WHERE email = :email", array(':email' => $content->email));
      $results = $query->fetch();
      $ifEntryExists = false;
      foreach($results as $c){
        $ifEntryExists = intval($c); // check if this email has already made his/her
      }
      if(!$ifEntryExists) {
        $result = $connection->insert('contact')
                                                ->fields([
                                                  'uuid' => REQUEST_TIME,
                                                  'email' => $content->email,
                                                  'changed' => REQUEST_TIME
                                                ])
                                                  ->execute();
        return new ResourceResponse(['success' => 'Subscribed'], 201);
      }
      else {
        return new ResourceResponse(['error' => 'This email has already subsccribed'],422);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }

  public function validate($content)
  {
    return isset($content->email) && !empty($content->email) && filter_var($content->email, FILTER_VALIDATE_EMAIL);
  }
}
