<?php
namespace Drupal\user_articles\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to get the current user`s saved Articles in the system.
 *
 * @RestResource(
 *   id = "get_user_articles_rest_resource",
 *   label = @Translation("Get User Articles rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/articles",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/articles"
 *   }
 * )
 */

class UserArticlesResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity get requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    $currentUser = \Drupal::currentUser();
    $user = \Drupal\user\Entity\User::load($currentUser->Id());
    $articles = array();
    $articles['id'] = array();
    $articles['title'] = array();
    foreach($user->field_user_articles as $a) {
      $target_id = $a->target_id;
      $article = \Drupal\node\Entity\Node::load($target_id);
      if(!is_null($article)):
        $articles['title'][] = $article->get('title')->value;
      endif;
    }
    return new ResourceResponse($articles);
  }
}
