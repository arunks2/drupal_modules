<?php
namespace Drupal\update_user\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to get the current user in the system.
 *
 * @RestResource(
 *   id = "update_user_rest_resource",
 *   label = @Translation("Update User rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/update",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/update"
 *   }
 * )
 */

class UpdateUserResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $currentUser = \Drupal::currentUser();
    $content = json_decode($request->getContent());
    $result = $this->validate($content);
    if(!$result) {
      return new ResourceResponse(['error'=>'Bad Request'], 400);
    }
    else {
      $user = \Drupal\user\Entity\User::load($currentUser->Id());
      $user->set('field_first_name',$content->first_name);
      $user->set('field_last_name',$content->last_name);
      $user->set('field_mobile_number',$content->mobile_number);
      $user->save();
      $temp_user['first_name'] = $user->get('field_first_name')->value;
      $temp_user['last_name'] = $user->get('field_last_name')->value;
      $temp_user['mobile_number'] = $user->get('field_mobile_number')->value;
      $data['user'] = $temp_user;
      return new ResourceResponse($data);
    }
  }

  public function validate($content)
  {
    return isset($content->first_name, $content->last_name, $content->mobile_number) && !empty($content->first_name) && !empty($content->last_name) && !empty($content->mobile_number);
  }
}
