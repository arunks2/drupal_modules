<?php
namespace Drupal\delete_recipe_book\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\node\Entity\Node;


/**
 * Provides a resource to delete recipe book for the current user in the system.
 *
 * @RestResource(
 *   id = "delete_recipe_book_rest_resource",
 *   label = @Translation("Delete Recipe Book rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/recipebook/delete",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/recipebook/delete"
 *   }
 * )
 */

class DeleteRecipeBookResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if(!$this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $user = \Drupal\user\Entity\User::load($currentUser->Id());
      foreach($user->field_recipe_boook_user as $delta => $item)  {
        if ($item->target_id == $content->recipebook_id) {
          unset($user->field_recipe_boook_user[$delta]);
          $user->save();
          break;
        }
      }
      return new ResourceResponse(['success' =>'deleted'], 201);
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }

  public function validate($content)
  {
    return isset($content->recipebook_id) && !empty($content->recipebook_id);
  }
}
