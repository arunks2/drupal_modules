<?php
namespace Drupal\update_profile\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use \Drupal\user\Entity\User;


/**
 * Provides a resource to update the profile for the current user in the system.
 *
 * @RestResource(
 *   id = "update_user_profile_rest_resource",
 *   label = @Translation("Update the User Profile rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/update",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/update"
 *   }
 * )
 */

class UpdateUserResource extends ResourceBase {
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }
  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if(!$this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $user = User::load($currentUser->Id());
      if(!$content->has_password) {
        $user->set('field_first_name',$content->first_name);
        $user->set('field_last_name',$content->last_name);
        $user->set('field_mobile_number',$content->mobile_number);
        $user->save();
        return new ResourceResponse(['success' => 'User has been updated'], 201);
      }
      else {
        if($this->validatePassword($content)) {
          $old_password = $user->getPassword();
          $password_hasher = \Drupal::service('password');
          $result = $password_hasher->check($content->old_password, $old_password);
          if(!$result) {
            return new ResourceResponse(['error' => 'Wrong_Password'], 403);
          }
          else {
            $user->setPassword($content->new_password);
            $user->save();
            return new ResourceResponse(['success' => 'Password updated successfully'], 201);
          }
        }
        else {
          return new ResourceResponse(['error' => 'Bad Request'], 400);
        }
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->first_name, $content->last_name, $content->has_password, $content->mobile_number) && !empty($content->first_name) && !empty($content->last_name) && !empty($content->mobile_number) && !empty($content->has_password);
  }
  public function validatePassword($content)
  {
    return isset($content->old_password, $content->new_password) && !empty($content->old_password) && !empty($content->new_password);
  }
}
