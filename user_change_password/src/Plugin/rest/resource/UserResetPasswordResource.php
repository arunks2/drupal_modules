<?php
namespace Drupal\user_change_password\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to verify otp and change password for the current user in the system.
 *
 * @RestResource(
 *   id = "verify_otp_code_and_change_password_rest_resource",
 *   label = @Translation("Verify the User code and Change Password rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/password/reset",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/password/reset"
 *   }
 * )
 */

class UserResetPasswordResource extends ResourceBase {
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
        $users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties(['mail' => $content->email, 'field_forget_password_code' => $content->otp]);
        $user = reset($users);
        if ($user) {
          $uid = $user->id();
          $user =  \Drupal::entityTypeManager()->getStorage('user')->load($uid);
          $this->changepassword($user,$content); // change password
          return new ResourceResponse(['success' => 'Your password has been updated successfully, login again with new password'], 200);
        }
        else {
          return new ResourceResponse(['error' => "NO_SUCH_USER_EXISTS_IN_SYSTEM"], 404);
        }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->otp, $content->email) && !empty($content->otp) && !empty($content->email) && filter_var($content->email, FILTER_VALIDATE_EMAIL);
  }
  public function changepassword($user,$content)
  {
      $user->setPassword($content->password);
      $user->set('field_forget_password_code','');
      $user->save();
      return true;
  }
}
