<?php
namespace Drupal\recipecollection_save\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\user\Entity\User;

/**
 * Provides a resource to save recipecollection for the current user in the system.
 *
 * @RestResource(
 *   id = "save_user_recipecollection_rest_resource",
 *   label = @Translation("Save User RecipeCollection rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/recipecollection/save",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/recipecollection/save"
 *   }
 * )
 */

class UserRecipeCollectionSaveResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      // TODO :: check if node is indeed a taxonomy of recipecollection type
      $node = \Drupal\taxonomy\Entity\Term::load($content->recipecollection_id);
      $user = \Drupal\user\Entity\User::load($currentUser->Id());
      $user->field_user_recipe_collections[] = $node;
      $user->save();
      return new ResourceResponse(['success' =>'created'], 201);
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->recipecollection_id) && !empty($content->recipecollection_id);
  }
}
