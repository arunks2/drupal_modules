<?php
namespace Drupal\create_recipe_book\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\node\Entity\Node;
use \Drupal\user\Entity\User;


/**
 * Provides a resource to create recipe book for the current user in the system.
 *
 * @RestResource(
 *   id = "create_recipe_book_rest_resource",
 *   label = @Translation("Create Recipe Book rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/recipebook/create",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/recipebook/create"
 *   }
 * )
 */

class CreateRecipeBookResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    $currentUser = \Drupal::currentUser();
    if($this->validate($content)) {
      $node = Node::create([
        'type'                      => 'recipe_books',
        'title'                     => $content->name,
        'field_recipe_book_name'    => $content->name,
        'field_recipe_boook_user'   => $currentUser->Id()
      ]);
      $node->save();
      $recipebook = \Drupal\node\Entity\Node::load($node->id());
      $user = \Drupal\user\Entity\User::load($currentUser->Id());
      $user->field_user_recipe_books[] = $recipebook;
      $user->save();
      $recipebook = $this->putRecipeInBook($recipebook, $content);
      return new ResourceResponse($recipebook, 201);
    }
    else {
      $recipebook = Node::load($content->recipebook_id);
      if($recipebook->type !== 'recipe_book') {
        return new ResourceResponse(['error' = >'Not a Recipe Book Entity'],422);
      }
      if($recipebook->get('field_recipe_boook_user')->first()->getValue()['target_id'] == $currentUser->Id()) {
          $recipebook = $this->putRecipeInBook($recipebook, $content);
          return new ResourceResponse($recipebook, 201);
      }
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->name) && !empty($content->name);
  }
  public function putRecipeInBook($recipebook, $content)
  {
    if(!$this->ifRecipeIsInBook($recipebook, $content->recipe_id)) {
      $recipe = Node::load($content->recipe_id);
      if($recipe->type == 'recipe') {
        return false;
      }
      $recipebook->field_recipe_bok_recipes[] = $recipe;
      $recipebook->save();
      return $recipebook->toArray();
    }
    else {
      return false;
    }
  }
  public function ifRecipeIsInBook($recipebook, $recipeId)
  {
    $exists = \Drupal::entityTypeManager()
                              ->getStorage('node')
                              ->loadByProperties(['nid' => $recipebook->id(),'field_recipe_bok_recipes' => $recipeId]);
    if($exists) {
      return true;
    }
    return false;
  }
}
