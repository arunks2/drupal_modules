<?php
namespace Drupal\user_saverecipecollection\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\user\Entity\User;
use \Drupal\taxonomy\Entity\Term;


/**
 * Provides a resource to save a recipecollection for the current user in the system.
 *
 * @RestResource(
 *   id = "save_user_recipecollection_rest_resource",
 *   label = @Translation("Save User RecipeCollection rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/recipecollection/save",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/recipecollection/save"
 *   }
 * )
 */

class UserSaveRecipeCollectionResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $user = User::load($currentUser->Id());
      $recipecollection  = Term::load($content->recipecollection_id);
      if($recipecollection->vid !=='recipe_collection') {
        return new ResourceResponse(['error' => 'Unprocessable entity'], 422);
      }
      $isCheckRecipecollectionPresent = false;
      $isCheckRecipecollectionPresent = $this->checkIfRecipeCollection($user);
      if($isCheckRecipecollectionPresent) {
        return new ResourceResponse(['error' =>'Resource already Present'],400);
      }
      else {
        $user->field_user_recipe_collections[] = $recipecollection;
        $user->save();
        return new ResourceResponse(['success'=>'created'], 201);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->recipecollection_id) && !empty($content->recipecollection_id);
  }
  public function checkIfRecipeCollection($user)
  {
    $isCheckRecipecollectionPresent = false;
    foreach($user->field_user_recipe_collections as $delta => $value) {
      if($value->target_id == $content->recipecollection_id) {
        $isCheckRecipecollectionPresent = true;
        break;
      }
    }
    return $isCheckRecipecollectionPresent;
  }
}
