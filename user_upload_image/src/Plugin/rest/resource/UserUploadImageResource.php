<?php
namespace Drupal\user_upload_image\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\user\Entity\User;


/**
 * Provides a resource to upload User Profile Image for the current user in the system.
 *
 * @RestResource(
 *   id = "upload_user_profile_image_rest_resource",
 *   label = @Translation("Upload User Profile Image rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/image/upload",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/image/upload"
 *   }
 * )
 */

class UserUploadImageResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }
  public static function parseMultipart() {
    return $_POST;
  }
  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    $currentUser = \Drupal::currentUser();
    $user = User::load($currentUser->Id());
    $uid = $currentUser->Id();
    $image_path = realpath($content->file);
    var_dump($image_path);
    die();
    $image_info = image_get_info($image_path);
    $file = new StdClass();
    $file->uid = $uid;
    $file->uri = $image_path;
    $file->filemime = $image_info['mime_type'];
    $file->status = 0; // Yes! Set status to 0 in order to save temporary file.
    $file->filesize = $image_info['file_size'];
    $validators = array(
      'file_validate_is_image' => array(),
      'file_validate_image_resolution' => array(variable_get('user_picture_dimensions', '200x200')),
      'file_validate_size' => array(variable_get('user_picture_file_size', '30') * 1024),
    );

    // here all the magic :)  
    $errors = file_validate($file, $validators);
    if (empty($errors)) {
      file_save($file);
      $edit['picture'] = $file;
      user_save($account, $edit);
    }
  }
}
