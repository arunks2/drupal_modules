<?php
namespace Drupal\save_comments\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\node\Entity\Node;
use Drupal\comment\Entity\Comment;

/**
 * Provides a resource to save comments in the system.
 *
 * @RestResource(
 *   id = "save_comments_rest_resource",
 *   label = @Translation("Save comments rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/comments",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/comments"
 *   }
 * )
 */

class  SaveCommentResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    $currentUser = \Drupal::currentUser();
    if($this->validate($content)) {
      $values = [
          'entity_type' => 'node',            
          'entity_id'   => $content->entity_id,                
          'field_name'  => $content->field_name,       
          'uid' => $currentUser->Id(),                        
          'comment_type' => 'reviews',        
          'subject' => $content->subject,  
          'comment_body' => $content->body,
          'field_rating' => $content->rating,            
          'status' => 0
        ];
        $comment = Comment::create($values);
        $comment->save();
        return new ResourceResponse(['success' =>'done'], 201);
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->entity_id,$content->field_name, $content->subject, $content->body,$content->rating) && !empty($content->entity_id) && !empty($content->field_name) && !empty($content->subject) && !empty($content->body) && !empty($content->rating);
  }
}
