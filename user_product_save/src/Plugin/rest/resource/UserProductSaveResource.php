<?php
namespace Drupal\user_product_save\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\user\Entity\User;
use \Drupal\node\Entity\Node;


/**
 * Provides a resource to save a product for the current user in the system.
 *
 * @RestResource(
 *   id = "save_user_product_rest_resource",
 *   label = @Translation("Save User product rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/product/save",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/product/save"
 *   }
 * )
 */

class UserProductSaveResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if(!$this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $user = User::load($currentUser->Id());
      $product  = Node::load($content->product_id);
      if($product->getType() !== 'product') {
        return new ResourceResponse(['error' = >'Not a product'],422);
      }
      $isProductPresent = $this->checkIfProductPresent($user);
      if($isProductPresent){
        return new ResourceResponse(['error' => 'Product already in the Wishlist'],400);
      }
      else {
        $user->field_wishlist_products[] = $product;
        $user->save();
        return new ResourceResponse(['success' =>'Product added to wishlist'], 201);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->product_id) && !empty($content->product_id);
  }
  public function checkIfProductPresent($user)
  {
    $isProductPresent = false;
    foreach($user->field_wishlist_products as $delta => $value) {
      if($value->target_id == $content->product_id) {
        $isProductPresent = true;
      }
    }
    return $isProductPresent;
  }
}
