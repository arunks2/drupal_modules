<?php
namespace Drupal\user_logout\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\jwt\Transcoder\JwtTranscoderInterface;
use Drupal\jwt\Transcoder\JwtTranscoder;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\JsonWebToken\JsonWebToken;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to logout the current user in the system.
 *
 * @RestResource(
 *   id = "user_logout_rest_resource",
 *   label = @Translation("Logout User rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/logout",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/logout"
 *   }
 * )
 */

class UserLogoutResource extends ResourceBase {
    /**
     * The JWT Transcoder service.
     *
     * @var \Drupal\jwt\Transcoder\JwtTranscoderInterface
     */
    protected $transcoder;

    /**
     * The event dispatcher.
     *
     * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
     */
    protected $eventDispatcher;


    /**
     * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
     *   The entity type manager
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param \Psr\Log\LoggerInterface $logger
     *   A logger instance.
     * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
     *   The config factory.
     * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
     *   The link relation type manager.
     */
    public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      LoggerInterface $logger) {
      parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
      $this->eventDispatcher = \Drupal::service('event_dispatcher');
      $this->transcoder = new JwtTranscoder(new \Firebase\JWT\JWT(), \Drupal::configFactory(), \Drupal::service('key.repository'));
    }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $raw_jwt = $this->getJwtFromRequest($request);
    // Decode JWT and validate signature.
    try {
      $jwt = $this->transcoder->decode($raw_jwt);
    }
    catch (JwtDecodeException $e) {
      throw new AccessDeniedHttpException($e->getMessage(), $e);
    }
    $validate = new JwtAuthValidateEvent($jwt);
    // Signature is validated, but allow modules to do additional validation.
    $this->eventDispatcher->dispatch(JwtAuthEvents::VALIDATE, $validate);
    $valid = new JwtAuthValidEvent($jwt);
    $this->eventDispatcher->dispatch(JwtAuthEvents::VALID, $valid);
    $user = $valid->getUser();

    if (!$user) {
      throw new AccessDeniedHttpException('Unable to load user from provided JWT.');
    }

    return new ResourceResponse(['logout']);
  }

}
