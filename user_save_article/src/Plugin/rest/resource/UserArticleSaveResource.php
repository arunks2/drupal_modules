<?php
namespace Drupal\user_save_article\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Provides a resource to save an article for the current user in the system.
 *
 * @RestResource(
 *   id = "save_user_article_rest_resource",
 *   label = @Translation("Save User Article rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/article/save",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/article/save"
 *   }
 * )
 */

class UserArticleSaveResource extends ResourceBase {
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if(!$this->validate) {
      $currentUser = \Drupal::currentUser();
      // TODO :: check if node is indeed an article
      $article = \Drupal\node\Entity\Node::load($content->article_id);
      if($article->getType() !== 'article') {
        return new ResourceResponse(['error' => 'Not an article'],422);
      }
      $user = \Drupal\user\Entity\User::load($currentUser->Id());
      $isArticlePresent = false;
      $isArticlePresent = $this->checkIfArticlePresent($user);
      if($isArticlePresent) {
        return new ResourceResponse(['error'=> 'Article already attached'], 400);
      }
      else {
        $user->field_user_articles[] = $article;
        $user->save();
        return new ResourceResponse(['success' => 'Article attached'],201);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->article_id) && !empty($content->article_id);
  }

  public function checkIfArticlePresent($user)
  {
    $isArticlePresent = false;
    foreach($user->field_user_articles as $delta => $value) {
      if($value->target_id == $content->article_id) {
        $isArticlePresent = true;
      }
    }
    return $isArticlePresent;
  }
}
