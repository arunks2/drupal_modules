<?php
namespace Drupal\save_stores\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use \Drupal\taxonomy\Entity\Term;
use \Drupal\node\Entity\Node;

/**
 * Provides a resource to save stores in the system.
 *
 * @RestResource(
 *   id = "save_stores_rest_resource",
 *   label = @Translation("Save Stores rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/stores/create",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/stores/create"
 *   }
 * )
 */

class SaveStoresResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      if($content->type == 'update') {
        $result = $this->updateStores($content);
        return new ResourceResponse(['success' => $result],200);
      }
      $state  =  \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $content->state_name]);
      $state = reset($state);
      // if(!$state ) {
      //   $state  = Term::create([
      //     'name' => $content->state_name,
      //     'vid'  => 'states'
      //   ]);
      // }
      $city = \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $content->city_name]);
      $city = reset($city);
      $has_city = false;
      // var_dump($state->get('field_has_cities')->entity['tid']);
      // die();
      foreach($state->get('field_has_cities')->entity as $delta => $item)  {
        if ($item->value == $city->toArray()['tid'][0]['value']) {
          $has_city = true;
          break;
        }
      }
      if(!$has_city) {
        $state->field_has_cities[] = $city;
        $state->save();
      }
      
      // if(!$city)  {
      //   $city = Term::create([
      //     'name' => $content->city_name,
      //     'vid'  => 'cities'
      //   ]);
      // }
      // $store = Node::create([
      //   'type'                      => 'stores',
      //   'title'                     => $content->store_name,
      //   'field_store_cities'        => $city,
      //   'field_store_full_address'  => $content->address,
      //   'field_store_lang'          => $content->lang,
      //   'field_store_mobile_number' => $content->mobile_number,
      //   'field_store_name'          => $content->store_name,
      //   'field_store_postal_code'   => $content->postal_code,
      //   'field_store_states'        => $state,
      //   'field_store_lat'           => $content->lat,
      //   "field_store_timings"       => $content->timings,
      // ]);
      // $store->save();
      // $store = $this->assignClosedOn($content->closed_on, $store);
      return new ResourceResponse(['success' =>'done'], 201);
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->store_name, $content->address, $content->city_name, $content->state_name, $content->postal_code, $content->lat, $content->lang, $content->mobile_number) && !empty($content->store_name) && !empty($content->address) && !empty($content->city_name) && !empty($content->state_name) && !empty($content->postal_code) && !empty($content->lat) && !empty($content->lang) && !empty($content->mobile_number);
  }

  public function assignClosedOn($days,$store)
  {
     foreach($days as $d){
      $day = \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $d]);
      $day = reset($day);
      $store->field_closed_on[] = $day;
      $store->save();
    }
    return $store;
  }
  public function updateStores($content)
  {
    $category  =  \Drupal::entityManager()->getStorage('taxonomy_term')->loadByProperties(['name' => $content->store_name]);
    $stores = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['field_store_mobile_number' => $content->mobile_number]);
    if($store = reset($stores)) {
      $store->field_store_categories = $category;
      $store->save();
      return true;
    }
  }
}
