<?php

namespace Drupal\subscriber\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the subscriber entity.
 *
 * @ContentEntityType(
 *   id = "subscriber",
 *   label = @Translation("Subscriber"),
 *   base_table = "subscriber",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "email" = "email",
 *     "created_at" = "created_at",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "add" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   links = {
 *     "canonical" = "/subcriber/{subcriber}",
 *     "add-page" = "/subcriber/add",
 *     "add-form" = "/subcriber/add/{subcriber_type}",
 *     "edit-form" = "/subcriber/{subcriber}/edit",
 *     "delete-form" = "/subcriber/{subcriber}/delete",
 *     "collection" = "/admin/content/subcribers",
 *   },
 *   admin_permission = "administer site configuration",
 *   bundle_entity_type = "subscriber_type",
 *   fieldable = TRUE,
 *   field_ui_base_route = "entity.subscriber_type.edit_form",
 * )
 */
class Subscriber extends ContentEntityBase implements ContentEntityInterface {
	public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

	  // Standard field, used as unique if primary index.
	  $fields['id'] = BaseFieldDefinition::create('integer')
	    ->setLabel(t('ID'))
	    ->setDescription(t('The ID of the Subscriber entity.'))
	    ->setReadOnly(TRUE);

	  // Standard field, unique outside of the scope of the current project.
	  $fields['uuid'] = BaseFieldDefinition::create('uuid')
	    ->setLabel(t('UUID'))
	    ->setDescription(t('The UUID of the Subscriber entity.'))
	    ->setReadOnly(TRUE);
	  $fields['email'] = BaseFieldDefinition::create('email')
	    ->setLabel(t('EMAIL'))
	    ->setDescription(t('The EMAIL of the Subscriber entity.'))
	    ->setReadOnly(TRUE);
	   $fields['created_at'] = BaseFieldDefinition::create('created')
	    ->setLabel(t('CREATED AT'))
	    ->setDescription(t('The Created of the Subscriber entity.'))
	    ->setReadOnly(TRUE);
	  return $fields;
	}
}