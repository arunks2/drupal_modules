<?php
namespace Drupal\subscriber\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
/**
 * Subscriber Type
 * 
 * @ConfigEntityType(
 *   id = "subscriber_type",
 *   label = @Translation("Subscriber Type"),
 *   bundle_of = "subscriber",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "email" = "email",
 *     "created_at" = "created_at",
 *   },
 *   config_prefix = "subscriber_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "email",
 *     "created_at",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\subscriber\Form\SubscriberTypeEntityForm",
 *       "add" = "Drupal\subscriber\Form\SubscriberTypeEntityForm",
 *       "edit" = "Drupal\subscriber\Form\SubscriberTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/structure/subscriber_type/{subscriber_type}",
 *     "add-form" = "/admin/structure/subscriber_type/add",
 *     "edit-form" = "/admin/structure/subscriber_type/{subscriber_type}/edit",
 *     "delete-form" = "/admin/structure/subscriber_type/{subscriber_type}/delete",
 *     "collection" = "/admin/structure/subscriber_type",
 *   }
 * )
 */
class SubscriberTypeEntity extends ConfigEntityBundleBase {}
