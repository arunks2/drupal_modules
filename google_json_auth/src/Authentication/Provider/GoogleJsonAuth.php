<?php

namespace Drupal\google_json_auth\Authentication\Provider;
// require_once 'vendor/google/apiclient/src/Google/Client.php';

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\user\UserAuthInterface;
use Google_Client;
use Symfony\Component\HttpFoundation\Request;
use Drupal\file\Entity\File;


/**
 * JWT Authentication Provider.
 */
class GoogleJsonAuth implements AuthenticationProviderInterface {
  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The user auth service.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected $userAuth;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a HTTP basic authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The user authentication service.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UserAuthInterface $user_auth, FloodInterface $flood, EntityManagerInterface $entity_manager) {
    $this->configFactory = $config_factory;
    $this->userAuth = $user_auth;
    $this->flood = $flood;
    $this->entityManager = $entity_manager;
  }

  public function applies(Request $request) {
    $content = json_decode($request->getContent());
    return isset($content->token, $content->email,$content->id) && !empty($content->token) && !empty($content->email) && !empty($content->id);
  }
  public function authenticate(Request $request) {
    $flood_config = \Drupal::configFactory()->getEditable('user.flood');
    $content = json_decode($request->getContent());
    $token = $content->token;
    $email = $content->email;
    $id = $content->id;
    $image_url = false;
    $client = new Google_Client(['client_id' => '376035557311-a2e0p0dbksj234rj2688vth2ar1r6085.apps.googleusercontent.com']);  // Specify the CLIENT_ID of the app that accesses the backend
    $payload = $client->verifyIdToken($token);
    if ($payload) {
      $image_url = $payload['picture'];
      $userid = $payload['sub'];
      $name = $payload['name'];
      if($userid == $id) 
      { // users is valid
        $users = $this->entityManager->getStorage('user')->loadByProperties(['mail' => $email, 'status' => 1]);
        $user = reset($users);
        if ($user) {
          $uid = $user->id();
          return $this->entityManager->getStorage('user')->load($uid);
        }
        else {
          $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
          $user = \Drupal\user\Entity\User::create();
          $password = $this->random_password(6);
          $user->setPassword($password);
          $user->enforceIsNew();
          $user->setEmail($email);
          $user->setUsername($email);
          $user->set('init', 'email');
          $user->set('langcode', $language);
          $user->set('preferred_langcode', $language);
          $user->set('preferred_admin_langcode', $language);
          $user->activate();
          $result = $user->save();
          $file = $this->uploadImage($image_url);
          $user->set('user_picture', array('target_id' => $file->id));
          $user->save();
          return $user;
        }
      }
      else {
        return [];
      }
    } 
    else {
      // Invalid ID token
      return [];
    }
  }
  public function random_password( $length = 8 ) {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
      $password = substr( str_shuffle( $chars ), 0, $length );
      return $password;
  }

  public function uploadImage($url) {
    $file_content = file_get_contents($file_image);
    $directory = '/sites/default/files/pictures/2018-05/';
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
    $file_image = file_save_data($file_content, $directory . basename($file_image),     FILE_EXISTS_REPLACE);
    return $file_image;
  }
}
