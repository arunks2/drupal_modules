<?php
namespace Drupal\user_verify_code\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\jwt\Authentication\Event\JwtAuthGenerateEvent;
use Drupal\jwt\Authentication\Event\JwtAuthEvents;
use Drupal\jwt\JsonWebToken\JsonWebToken;


/**
 * Provides a resource to verify otp for the current user in the system.
 *
 * @RestResource(
 *   id = "verify_otp_code_rest_resource",
 *   label = @Translation("Verify the User code rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/user/code/verify",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/user/code/verify"
 *   }
 * )
 */

class UserCodeVerificationResource extends ResourceBase {
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity post requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent());
    if($this->validate($content)) {
      $currentUser = \Drupal::currentUser();
      $user = \Drupal\user\Entity\User::load($currentUser->Id());
      $result = $this->verifyEmail($user,$content);
      if($result) {
        return new ResourceResponse(['success' => 'Email verified'], 200);
      }
      else {
        return new ResourceResponse(['error' => 'OTP mismatch'], 422);
      }
    }
    else {
      return new ResourceResponse(['error' => 'Bad Request'], 400);
    }
  }
  public function validate($content)
  {
    return isset($content->otp) && !empty($content->otp);
  }
  public function verifyEmail($user, $content)
  {
    $otp = $user->get('field_email_verification_code')->value;
    if($content->otp == $otp) {
      $user->set('field_email_verification_code','');
      $user->set('field_is_email_valid',1);
      $user->save();
      return true;
    }
    else{
      return false;
    }
  }
}
