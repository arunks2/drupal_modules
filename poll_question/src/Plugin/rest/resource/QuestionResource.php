<?php
namespace Drupal\poll_question\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Drupal\Core\Database\Connection;
use Drupal\rest\ResourceResponse;


/**
 * Provides a resource to get a poll question.
 *
 * @RestResource(
 *   id = "poll_question_rest_resource",
 *   label = @Translation("Poll Question rest resource"),
 *   uri_paths = {
 *     "canonical" = "/api/v1/poll/questions",
 *     "https://www.drupal.org/link-relations/create" = "/api/v1/poll/questions"
 *   }
 * )
 */

class QuestionResource extends ResourceBase {
    /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $link_relation_type_manager
   *   The link relation type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    $serializer_formats,
    LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
  }

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    $connection = \Drupal::database();
    // $query = $connection->query("SELECT id, example FROM {mytable}");
    $query = $connection->query("SELECT * FROM {poll_field_data} WHERE status = 1 limit 1");
    $results = $query->fetchAll();
    foreach($results as $result) {
      $data['question'] = $result->question;
      $data['id'] = $result->id;
    }
    $choices = array();
    $query = $connection->query("Select id, choice FROM {poll_choice_field_data} as pcfd LEFT JOIN {poll__choice} as pc ON pcfd.id = pc.choice_target_id WHERE pc.entity_id = :id", array(':id' => $data['id']));
    $results = $query->fetchAll();
    $choice['id'] = array();
    $choice['choice'] = array();
    $choice['votes'] = array();
    foreach($results as $result) {
      array_push($choice['id'], $result->id);
      $query = $connection->query("select COUNT(*) FROM {poll_vote} where chid =:id",array(':id' => $result->id));
      $count = $query->fetch();
      foreach($count as $c){
        $vote = $c;
      }
      array_push($choice['votes'], $vote);
      array_push($choice['choice'], $result->choice);
    }
    $data['choices'] = $choice;
    return new ResourceResponse($data);
  }
  
}
